<?php
class Troco{
	public function __construct(){}

	public function getQtdeNotas(float $money){

		$arrayDeNotas = [10000,5000,2000,1000,500,200,100,50,25,10,5,1];
		$dinheiro = $money * 100;

		foreach ($arrayDeNotas as $value){
			$notas["" . ($value/100)] = intdiv($dinheiro, $value);
			$dinheiro = $dinheiro % $value;	
		}

		return $notas;
	}
}

$troco = new Troco();
$notas = $troco->getQtdeNotas(125);
print_r($notas);	
